Readme Template https://gist.github.com/jxson/1784669

## Synopsis

Ini adalah kelompok _assets digunakan di seluruh aplikasi turunan lwscodeigniterwrapper.
Direktori ini akan mengandung file-file seperti css, dan js.

## Code Example

--

## Motivation

A short description of the motivation behind the creation and maintenance of the project. 
This should explain **why** the project exists.

## Installation

Provide code examples and explanations of how to get the project.

## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

## Tests

Describe and show how to run the tests with code examples.

## Contributors

Let people know how they can dive into the project, include important links to things like issue trackers, irc, twitter accounts if applicable.

## License

--