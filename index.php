<?php

define('ASSET_UPLOAD', dirname(__FILE__));

/**
 * set value TRUE, FALSE (boolean)
 */
define('ONCPANEL', FALSE);
define('ONWRAPPER', TRUE);

define('APPPATH', dirname(__FILE__) . DIRECTORY_SEPARATOR);
require_once 'autoload.php';

/* End of file index.php */
/* Location: ./index.php */
?>